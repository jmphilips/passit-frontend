export interface IRegisterForm {
  email: string;
  password: string;
  passwordConfirm: string;
  showConfirm: boolean;
  signUpNewsletter: boolean;
  rememberMe?: boolean;
}

export interface IUrlForm {
  url: string;
}

