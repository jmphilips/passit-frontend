import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { Observable, ReplaySubject } from "rxjs";
import { cold, hot } from "jasmine-marbles";

import {
  LogoutSuccessAction,
  HandleAPIErrorAction,
  UserMustConfirmEmailAction,
  LogoutAction
} from "./account.actions";
import { LoginEffects } from "./account.effects";
import { UserService } from "./user";
import { StoreModule, combineReducers } from "@ngrx/store";

import * as fromRoot from "../app.reducers";
import * as fromAccount from "./account.reducer";
import { HttpErrorResponse } from "@angular/common/http";

describe("Account Effects", () => {
  let effects: LoginEffects;
  let actions: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          feature: combineReducers(fromAccount.reducers)
        })
      ],
      providers: [
        LoginEffects,
        provideMockActions(() => actions),
        {
          provide: UserService,
          useValue: jasmine.createSpyObj("UserService", ["login"])
        }
      ]
    });

    effects = TestBed.get(LoginEffects);
  });

  it("logout should remove auth localstorage", () => {
    localStorage.setItem("auth", "fake");

    actions = new ReplaySubject(1);
    (actions as any).next(new LogoutSuccessAction());

    effects.logout$.subscribe(() => {
      expect(localStorage.getItem("auth")).toBe(null);
    });
  });

  it("handleAPIError should handle 403 users email not confirmed", () => {
    const nothing: any = [];
    const resp: HttpErrorResponse = {
      status: 403,
      statusText: "Forbidden",
      error: {
        detail: "User's email is not confirmed."
      },
      ok: false,
      type: nothing,
      headers: nothing,
      name: "HttpErrorResponse",
      url: "http://127.0.0.1:8000/api/groups/",
      message:
        "Http failure response for http://127.0.0.1:8000/api/groups/: 403 Forbidden"
    };
    const error = {
      res: resp
    };
    actions = hot("a", { a: new HandleAPIErrorAction(error) });
    const expected = cold("b", { b: new UserMustConfirmEmailAction() });
    expect(effects.handleAPIError$).toBeObservable(expected);
  });

  it("handleAPIError should handle 403 user is not logged in", () => {
    const nothing: any = [];
    const resp: HttpErrorResponse = {
      status: 403,
      statusText: "Forbidden",
      error: {
        detail: "Invalid token."
      },
      ok: false,
      type: nothing,
      headers: nothing,
      name: "HttpErrorResponse",
      url: "http://127.0.0.1:8000/api/groups/",
      message:
        "Http failure response for http://127.0.0.1:8000/api/groups/: 403 Forbidden"
    };
    const error = {
      res: resp
    };
    actions = hot("a", { a: new HandleAPIErrorAction(error) });
    const expected = cold("b", { b: new LogoutAction() });
    expect(effects.handleAPIError$).toBeObservable(expected);
  });
});
