import { LoginFailureAction } from "../account.actions";
import { initialState, reducer } from "./login.reducer";

describe("LoginReducer", () => {
  describe("LOGIN_FAILURE", () => {
    it("should return error message", () => {
      const error = "some error";
      const createAction = new LoginFailureAction(error);
      const result = reducer(initialState, createAction);
      expect(result.errorMessage).toBe(error);
      expect(result.hasFinished).toBe(false);
      expect(result.hasStarted).toBe(false);
    });
  });
});
