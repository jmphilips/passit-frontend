export interface ILoginForm {
  email: string;
  password: string;
  rememberMe?: boolean;
  url?: string;
}
