import {
  FormGroupState,
  createFormGroupState,
  validate,
  markAsSubmitted,
  createFormGroupReducerWithUpdate,
  setValue
} from "ngrx-forms";
import { required, pattern } from "ngrx-forms/validation";
import { environment } from "../../../environments/environment";

import { AccountActions, AccountActionTypes } from "../account.actions";
import { ILoginForm } from ".";
import { DEFAULT_API } from "../../constants";
import { oldPasswordValidators } from "../constants";

const FORM_ID = "Login Form";

const initialFormState = createFormGroupState<ILoginForm>(FORM_ID, {
  email: "",
  password: "",
  rememberMe: environment.extension || environment.nativescript ? true : false,
  url: environment.extension || environment.nativescript ? DEFAULT_API : ""
});

export interface ILoginState {
  form: FormGroupState<ILoginForm>;
  hasStarted: boolean;
  hasFinished: boolean;
  errorMessage: string | null;
}

export const initialState: ILoginState = {
  form: initialFormState,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null
};

export const formReducer = createFormGroupReducerWithUpdate<ILoginForm>({
  email: validate([required, pattern(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/)]),
  password: validate<string>(oldPasswordValidators)
});

export function reducer(
  state = initialState,
  action: AccountActions
): ILoginState {
  const form = formReducer(state.form, action);
  if (form !== state.form) {
    state = { ...state, form };
  }

  switch (action.type) {
    case AccountActionTypes.LOGIN_REDIRECT:
      if (action.payload) {
        form.controls.email = setValue(action.payload, form.controls.email);
      }
      return {
        ...state,
        form
      };

    case AccountActionTypes.LOGIN:
      return {
        ...state,
        form: markAsSubmitted(state.form),
        hasStarted: true,
        hasFinished: false,
        errorMessage: null
      };

    case AccountActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        hasStarted: false,
        hasFinished: true
      };

    case AccountActionTypes.LOGIN_FAILURE:
      return {
        ...state,
        hasStarted: false,
        errorMessage: action.payload
      };

    default:
      return state;
  }
}

export const getHasStarted = (state: ILoginState) => state.hasStarted;
export const getHasFinished = (state: ILoginState) => state.hasFinished;
export const getErrorMessage = (state: ILoginState) => state.errorMessage;
export const getForm = (state: ILoginState) => state.form;
