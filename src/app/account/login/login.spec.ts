import { ComponentFixture, TestBed, async } from "@angular/core/testing";
import { InlineSVGModule } from "ng-inline-svg";

import { ProgressIndicatorComponent } from "../../progress-indicator";
import { LoginComponent } from "./login.component";
import { NgrxFormsModule } from "ngrx-forms";

import { StoreModule } from "@ngrx/store";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { LoginContainer } from ".";

import * as fromAccount from "../account.reducer";
import * as fromRoot from "../../app.reducers";
import { RouterTestingModule } from "@angular/router/testing";

describe("LoginComponent", () => {
  let component: LoginContainer;
  let fixture: ComponentFixture<LoginContainer>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProgressIndicatorComponent,
        LoginContainer,
        LoginComponent
      ],
      imports: [
        InlineSVGModule.forRoot(),
        NgrxFormsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("account", fromAccount.reducers)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginContainer);
    component = fixture.componentInstance;
  });

  it("should exist", () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it("should display validation errors if the form is submitted empty", () => {
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#loginSubmit").click();
    fixture.detectChanges();
    expect(
      fixture.nativeElement.innerText.indexOf(
        "Enter your account's email address."
      ) !== -1
    ).toBe(true);
  });
});
