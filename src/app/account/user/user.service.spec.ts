import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { StoreModule } from "@ngrx/store";

import { User } from "./user";
import { UserService } from "./user.service";

import { Store } from "@ngrx/store";
import { IState } from "../../app.reducers";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { reducers } from "../account.reducer";
import { getUrl } from "../account.reducer";

const sdkStub = {
  is_username_available: () => {
    return Promise.resolve(true);
  }
};

describe("Service: UserService", () => {
  let service: UserService;
  let store: Store<IState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature("account", reducers)
      ],
      providers: [{ provide: NgPassitSDK, useValue: sdkStub }, UserService]
    });
    service = TestBed.get(UserService);
    store = TestBed.get(Store);
  });

  it("Can set up SDK", done => {
    const user: User = {
      email: "test@example.com",
      password: "mypass"
    };

    service.checkUsername(user.email).then(isAvail => {
      expect(isAvail.isAvailable).toBe(true);
      done();
    });
  });

  it("Can check and set url", () => {
    const correctUrl = "https://api.passit.io/api/";
    // mockIt(mockBackend, {
    //   body: "{}",
    //   url: correctUrl,
    // });
    const url = "passit.io"; // The service should prefix https and api
    service.checkAndSetUrl(url).then(() => {
      store.select(getUrl).subscribe(storeUrl => {
        expect(storeUrl).toBe(correctUrl);
      });
    });
  });
});
