export interface IAuthStore {
  publicKey: string;
  privateKey: string;
  userId: number;
  email: string;
  userToken: string;
  rememberMe: boolean;
}
