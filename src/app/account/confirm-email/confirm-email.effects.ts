import "rxjs/add/operator/do";
import "rxjs/add/operator/exhaustMap";
import "rxjs/add/operator/map";

import { Injectable } from "@angular/core";
import { Actions, Effect } from "@ngrx/effects";

import { UserService } from "../user";

import {
  ResetRegisterCodeFailureAction,
  ResetRegisterCodeSuccessAction
} from "./confirm-email.actions";

import * as ConfirmEmail from "./confirm-email.actions";

@Injectable()
export class ConfirmEmailEffects {
  @Effect()
  ResetRegisterCode$ = this.actions$
    .ofType(ConfirmEmail.RESET_REGISTER_CODE)
    .switchMap(() => {
      return this.userService
        .resetRegisterCode()
        .then((resp: any) => new ResetRegisterCodeSuccessAction())
        .catch((error: any) => new ResetRegisterCodeFailureAction());
    });

  constructor(private actions$: Actions, private userService: UserService) {}
}
