import * as ConfirmEmail from "./confirm-email.actions";

export interface IConfirmEmailState {
  resetCodeMessage: string | null;
}

export const initialState: IConfirmEmailState = {
  resetCodeMessage: null
};

export function reducer(
  state = initialState,
  action: ConfirmEmail.Actions
): IConfirmEmailState {
  switch (action.type) {
    case ConfirmEmail.RESET_REGISTER_CODE:
      return {
        ...state,
        resetCodeMessage: null
      };

    case ConfirmEmail.RESET_REGISTER_CODE_SUCCESS:
      return {
        ...state,
        resetCodeMessage: "Check your email for a new confirmation code."
      };

    case ConfirmEmail.RESET_REGISTER_CODE_FAILURE:
      return {
        ...state,
        resetCodeMessage: "Unable to resend confirmation code."
      };

    default:
      return state;
  }
}

export const getResetCodeMessage = (state: IConfirmEmailState) =>
  state.resetCodeMessage;
