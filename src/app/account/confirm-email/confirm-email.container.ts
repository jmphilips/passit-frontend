import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";

import * as fromRoot from "../../app.reducers";
import { NgPassitSDK } from "../../ngsdk/sdk";
import * as fromAccount from "../account.reducer";
import * as emailActions from "./confirm-email.actions";
import { IncrementStageAction } from "../register/register.actions";

/**
 * Confirm email container
 */
@Component({
  selector: "confirm-email-container",
  template: `
    <confirm-email-component
      [hasStarted]="hasStarted"
      [hasFinished]="hasFinished"
      [errorMessage]="errorMessage"
      [confirmCodeMessage]="confirmCodeMessage$ | async"
      [code]="code"
      (confirmEmail)="confirmEmail($event)"
      (resetRegisterCode)="resetRegisterCode()"
      [inline]="inline"
    ></confirm-email-component>
  `
})
export class ConfirmEmailContainer implements OnInit {
  /**
   * The error message, passed to the template
   */
  errorMessage: string;

  confirmCodeMessage$ = this.store.select(fromAccount.getConfirmEmailMessage);

  /**
   * Lets the template know if a server call has started
   //  */
  hasStarted = false;

  /**
   * Lets the template know if a server call has finished (successfully)
   */
  hasFinished = false;

  /**
   * The confirmation code
   */
  code = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ngPassitSDK: NgPassitSDK,
    private store: Store<fromRoot.IState>
  ) {}

  /**
   * Confirm email will temporarily appear in two different ways which requires
   * logic to distinguish
   */
  @Input() inline: boolean;

  /**
   * On init, look for the code param in the URL. If it's there, submit it.
   */
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.code = params["code"];
      if (params["code"]) {
        this.confirmLongCode(params["code"]);
      }
    });
  }

  /**
   * Confirm a long code - authentication not required.
   * @param code long code from email web link
   */
  confirmLongCode(code: string) {
    // this.hasStarted = true;
    this.ngPassitSDK
      .confirm_email_long_code(code)
      .then(() => {
        // this.hasStarted = false;
        // this.hasFinished = true;
        this.router.navigate(["/list"]);
      })
      .catch(err => {
        // this.hasStarted = false;
        // this.hasFinished = false;
      });
  }

  /**
   * Submit function
   * If code submits successfully, redirect away.
   * Otherwise, throw an error that the template can read
   *
   * @param code The code used to confirm the email address
   */
  confirmEmail(code: string) {
    if (typeof code === "undefined" || code === "") {
      return (this.errorMessage = "Please enter your code here.");
    } else {
      this.hasStarted = true;
      this.ngPassitSDK
        .confirm_email_short_code(code)
        .then(resp => {
          this.hasStarted = false;
          this.hasFinished = true;
          this.inline
            ? this.store.dispatch(new IncrementStageAction())
            : this.router.navigate(["/list"]);
        })
        .catch(res => {
          this.hasStarted = false;
          this.hasFinished = false;
          const status = res.res.status;
          if (status === 404 || status === 400) {
            return (this.errorMessage =
              "This code doesn't match what we sent. Please try again.");
          } else if (status === 406) {
            return (this.errorMessage = `We invalidated this code because of too many failed attempts.
            Press the "Resend confirmation" link below to receive a new code.`);
          } else if (status === 403) {
            return (this.errorMessage =
              "To validate via confirmation code, please log in first.");
          }
          this.errorMessage = "Unexpected error.";
        });
    }
  }

  resetRegisterCode() {
    this.store.dispatch(new emailActions.ResetRegisterCodeAction());
  }
}
