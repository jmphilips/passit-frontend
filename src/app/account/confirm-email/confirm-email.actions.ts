import { Action } from "@ngrx/store";

export const RESET_REGISTER_CODE = "ResetRegisterCode";
export const RESET_REGISTER_CODE_SUCCESS = "[ResetRegisterCode] Success";
export const RESET_REGISTER_CODE_FAILURE = "[ResetRegisterCode] Failure";

export class ResetRegisterCodeAction implements Action {
  readonly type = RESET_REGISTER_CODE;
}

export class ResetRegisterCodeSuccessAction implements Action {
  readonly type = RESET_REGISTER_CODE_SUCCESS;
}

export class ResetRegisterCodeFailureAction implements Action {
  readonly type = RESET_REGISTER_CODE_FAILURE;
}

export type Actions =
  | ResetRegisterCodeAction
  | ResetRegisterCodeSuccessAction
  | ResetRegisterCodeFailureAction;
