import { INewSecret } from "passit-sdk-js/js/sdk.interfaces";

export interface ImportableSecret extends INewSecret {
  doImport: boolean;
  importable: boolean;
}
