import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "navbar",
  styleUrls: ["./navbar.component.scss"],
  templateUrl: "./navbar.component.html"
})
export class NavbarComponent {
  @Input() isLoggedIn: boolean;
  @Input() isPrivateOrgMode: boolean;
  @Output() logout = new EventEmitter();
}
