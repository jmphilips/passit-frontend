import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { InlineSVGModule } from "ng-inline-svg";

import { DemoComponent, NavbarComponent, NavbarContainer } from "./navbar/";
import { SearchComponent } from "./search";

export const COMPONENTS = [
  NavbarContainer,
  NavbarComponent,
  SearchComponent,
  DemoComponent
];

@NgModule({
  imports: [CommonModule, RouterModule, InlineSVGModule],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class SharedModule {}
