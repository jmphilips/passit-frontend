import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { StoreModule } from "@ngrx/store";

import { reducers } from "../../account/account.reducer";
import { Api } from "../../ngsdk/api";
import { NgPassitSDK } from "../../ngsdk/sdk";
import { ContactsService } from "./contacts.service";

describe("Contacts Service", () => {
  let service: ContactsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({}),
        // the api checks for auth...even though we dont need it.
        StoreModule.forFeature("account", reducers)
      ],
      providers: [Api, NgPassitSDK, ContactsService]
    });
    service = TestBed.get(ContactsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it("look up an id by email", () => {
    service.contactLookup("test@example.com").map(id => {
      expect(id).toBe(1);
    });
    const req = httpMock.expectOne("http://127.0.0.1:8000/api/lookup-user-id/");
    req.flush({ id: 1 });
  });
});
