import { createSelector } from "@ngrx/store";
import { setContacts } from "../../utils/contacts";
import { ContactsActionTypes, ContactsActionsUnion } from "./contacts.actions";
import { IContact } from "./contacts.interfaces";

export interface IContactsState {
  contacts: IContact[];
}

const initialState: IContactsState = {
  contacts: []
};

export function contactsReducer(
  state = initialState,
  action: ContactsActionsUnion
): IContactsState {
  switch (action.type) {
    case ContactsActionTypes.SET_CONTACTS:
      return Object.assign({}, state, {
        contacts: action.payload
      });

    default:
      return state;
  }
}

export const getContacts = (state: IContactsState) => state.contacts;

export const getDisplayContacts = createSelector(getContacts, contactList =>
  setContacts(contactList, false)
);
