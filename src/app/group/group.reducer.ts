import { createFeatureSelector, createSelector } from "@ngrx/store";
import {
  ResetFormActionTypes,
  ResetFormActionsUnion
} from "../form/reset-form.actions";
import { setContacts } from "../utils/contacts";
import { sortList } from "../utils/sort";
import {
  ContactsActionsUnion,
  ContactsActionTypes
} from "./contacts/contacts.actions";
import { ISelectOptions } from "./group-form/group-form.interfaces";
import { GroupActionsUnion, GroupActionTypes } from "./group.actions";
import { IGroup } from "./group.interfaces";
import { IGroupForm } from "./group.interfaces";
import { IContact } from "./contacts/contacts.interfaces";
// import { getUserId } from "../account/account.reducer";

export interface IGroupState {
  groupForm: IGroupForm;
  groupManaged?: number | null;
  groups: IGroup[];
  showCreate: boolean;
  groupIsUpdating: boolean;
  groupIsUpdated: boolean;
  contactLookup: ISelectOptions | null;
  newContacts: IContact[];
  contacts: IContact[];
}

export const initialState: IGroupState = {
  groupForm: {
    name: "",
    members: []
  },
  groupManaged: null,
  groups: [],
  showCreate: false,
  groupIsUpdating: false,
  groupIsUpdated: false,
  contactLookup: null,
  newContacts: [],
  contacts: []
};

export function groupReducer(
  state = initialState,
  action: ResetFormActionsUnion | GroupActionsUnion | ContactsActionsUnion
): IGroupState {
  switch (action.type) {
    case GroupActionTypes.ADD_GROUP: {
      return Object.assign({}, state, {
        groups: [...state.groups, action.payload]
      });
    }

    case GroupActionTypes.REMOVE_GROUP: {
      const groups = state.groups.filter(g => {
        return g.id !== action.payload;
      });
      return Object.assign({}, state, {
        groups
      });
    }

    case GroupActionTypes.RESET_BLANK_GROUP_FORM: {
      return Object.assign({}, state, {
        groupForm: initialState.groupForm
      });
    }

    case GroupActionTypes.SET_GROUPS:
      return Object.assign({}, state, {
        groups: action.payload
      });

    case GroupActionTypes.GET_GROUPS:
      return Object.assign({}, state, {
        groups: state.groups
      });

    // Get all existing contacts from the API
    case ContactsActionTypes.SET_CONTACTS:
      return Object.assign({}, state, {
        contacts: action.payload
      });

    case GroupActionTypes.SHOW_GROUPS_CREATE:
      return Object.assign({}, state, {
        showCreate: true,
        contactLookup: null,
        groupIsUpdating: false,
        groupIsUpdated: false,
        groupForm: {
          members: [],
          name: ""
        }
      });

    case GroupActionTypes.CREATE_GROUP:
      return Object.assign({}, state, {
        groupIsUpdating: true
      });

    case GroupActionTypes.CREATE_GROUP_SUCCESS:
      return Object.assign({}, state, {
        groupIsUpdating: false,
        groupIsUpdated: true,
        showCreate: false,
        contactLookup: null
      });

    case GroupActionTypes.HIDE_GROUPS_CREATE:
      return Object.assign({}, state, {
        showCreate: false,
        groupForm: initialState.groupForm
      });

    case GroupActionTypes.UPDATE_GROUP:
      return Object.assign({}, state, {
        groupIsUpdating: true
      });

    case GroupActionTypes.UPDATE_GROUP_SUCCESS:
      return Object.assign({}, state, {
        groupIsUpdating: false,
        groupIsUpdated: true,
        contactLookup: null
      });

    case GroupActionTypes.CLEAR_MANAGED_GROUP:
      return Object.assign({}, state, {
        groupManaged: null,
        groupForm: initialState.groupForm
      });

    case ResetFormActionTypes.RESET_FORMS:
      return Object.assign({}, state, {
        ...initialState,
        groups: state.groups,
        contacts: state.contacts
      });

    case GroupActionTypes.UPDATE_FORM:
      return Object.assign({}, state, {
        groupForm: {
          name: action.payload.name,
          members: action.payload.members
        }
      });

    case GroupActionTypes.SET_MANAGED_GROUP:
      const groupId = action.payload;
      const foundGroup = state.groups.find(
        groupSearched => groupSearched.id === groupId
      );
      const users = foundGroup!.groupuser_set.map(
        groupMember => groupMember.user
      );
      return Object.assign({}, state, {
        groupManaged: groupId,
        groupIsUpdating: false,
        groupIsUpdated: false,
        groupForm: {
          name: foundGroup!.name,
          members: users
        }
      });

    case GroupActionTypes.CONTACT_LOOKUP_SUCCESS:
      const payload = action.payload;
      return Object.assign({}, state, {
        contactLookup: payload
      });

    // Add selected contact to contact list
    case GroupActionTypes.CONTACT_RESET:
      const selectedMember = action.payload;
      const newContact = {
        id: selectedMember.value,
        email: selectedMember.label,
        first_name: "",
        last_name: ""
      };
      return Object.assign({}, state, {
        contacts: state.contacts.concat(newContact),
        contactLookup: null
      });

    case GroupActionTypes.CONTACT_LOOKUP_FAILURE:
      return Object.assign({}, state, {
        contactLookup: null
      });

    case GroupActionTypes.ROUTER_UPDATE_LOCATION:
      return Object.assign({}, state, {
        groupManaged: null,
        showCreate: false
      });

    default:
      return state;
  }
}

export const getGroupState = createFeatureSelector<IGroupState>("group");

export const getGroups = createSelector(
  getGroupState,
  (state: IGroupState) => state.groups
);

// export const getPendingGroups = createSelector(
//   getGroups,
//   getUserId,
//   (groups, userId) =>
//     groups.filter(group =>
//       group.groupuser_set.find(
//         groupuser => groupuser.user === userId && groupuser.is_invite_pending
//       )
//     )
// );

export const getSortedGroups = createSelector(
  getGroupState,
  (state: IGroupState) => sortList(state.groups)
);

export const getGroupShowCreate = createSelector(
  getGroupState,
  (state: IGroupState) => state.showCreate
);
export const getGroupManaged = createSelector(
  getGroupState,
  (state: IGroupState) => state.groupManaged
);
export const getGroupForm = createSelector(
  getGroupState,
  (state: IGroupState) => state.groupForm
);
export const getGroupIsUpdating = createSelector(
  getGroupState,
  (state: IGroupState) => state.groupIsUpdating
);
export const getGroupIsUpdated = createSelector(
  getGroupState,
  (state: IGroupState) => state.groupIsUpdated
);
export const getContactLookup = createSelector(
  getGroupState,
  (state: IGroupState) => state.contactLookup
);

const getContacts = createSelector(
  getGroupState,
  (state: IGroupState) => state.contacts
);
const getNewContacts = createSelector(
  getGroupState,
  (state: IGroupState) => state.newContacts
);

/** Get contacts plus "new contacts" which are ones found from looking them up by email */
export const getCombinedContacts = createSelector(
  getContacts,
  getNewContacts,
  (contacts, newContacts) => contacts.concat(newContacts)
);

/** TODO refactor this, not sure what it does */
export const getAllContacts = createSelector(
  getGroupState,
  (state: IGroupState) =>
    state.contacts.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj.email).indexOf(obj.email) === pos;
    })
);

/* Return contacts for UI display. If searched contact exists, return all
   contacts plus searched contact.
*/
export const getGroupMembersForDisplay = createSelector(
  getAllContacts,
  getContactLookup,
  (members, searchedContact) => {
    if (searchedContact) {
      const searchedMembersArray: ISelectOptions[] = setContacts(members, true);
      const duplicateContact = searchedMembersArray.find(
        contact => contact.label === searchedContact.label
      );
      if (duplicateContact) {
        searchedMembersArray.map(contact => {
          contact !== duplicateContact
            ? (contact.disabled = true)
            : (contact.disabled = false);
        });
        return searchedMembersArray;
      }
      return searchedMembersArray.concat(searchedContact);
    }
  }
);

// Return all contacts for UI display.
export const getGroupContacts = createSelector(getAllContacts, members => {
  const membersArray: ISelectOptions[] = setContacts(members, false);
  return membersArray;
});
