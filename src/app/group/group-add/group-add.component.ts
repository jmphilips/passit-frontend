import { Component, EventEmitter, Input, Output } from "@angular/core";

import { IContact } from "../contacts/contacts.interfaces";
import { IGroup } from "../group.interfaces";
import { IGroupForm } from "../group.interfaces";

interface ISelectOptions {
  label: string;
  value: any;
}

@Component({
  selector: "group-add",
  styleUrls: [
    "../../list/secret-row/secret-row.component.scss",
    "../../list/list.component.scss"
  ],
  template: `
    <h2 class="secret__title secret__title--add">
      Add New Group
    </h2>

    <group-form
      isNew="true"
      [contacts]="contacts"
      [groupIsUpdating]="groupIsUpdating"
      [groupIsUpdated]="groupIsUpdated"
      [groupForm]="groupForm"
      [isPrivateOrgMode]="isPrivateOrgMode"
      [groupContacts]="groupContacts"
      [searchedContacts]="searchedContacts"
      [contactLookup]="contactLookup"
      (hideAddSecretForm)="hideAddSecretForm.emit()"
      (keyup)="contactSearch.emit($event.target.value)"
      (save)="onSave()"
      (updateFormValues)="updateFormValues.emit($event)"
      (resetSearch)="resetSearch.emit($event)"
      class="secret-form secret-form--is-active"
    ></group-form>
  `
})
export class GroupAddComponent {
  group: IGroup;
  @Input() contacts: IContact[];
  @Input() groupIsUpdating: boolean;
  @Input() groupIsUpdated: boolean;
  @Input() searchedContacts: ISelectOptions[];
  @Input() isPrivateOrgMode: boolean;
  @Input() contactLookup: ISelectOptions[];
  @Input() groupContacts: ISelectOptions[];
  @Input() groupForm: IGroupForm;

  @Output() saveNew = new EventEmitter();
  @Output() hideAddSecretForm = new EventEmitter();
  @Output() contactSearch = new EventEmitter<string>();
  @Output() resetSearch = new EventEmitter();
  @Output() updateFormValues = new EventEmitter<IGroupForm>();

  public onGroupCreated: EventEmitter<boolean>;

  public onSave() {
    this.saveNew.emit();
  }
}
