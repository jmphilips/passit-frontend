export interface ISelectOptions {
  label: string;
  value: any;
  disabled: boolean;
}
