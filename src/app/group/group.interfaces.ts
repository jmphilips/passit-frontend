import * as api from "passit-sdk-js/js/api.interfaces";

export interface IGroupForm {
  id?: number;
  name?: string;
  slug?: string;
  members: number[];
}

export interface IGroup extends api.IGroup {
  slug: string;
}
