import { PopupActions, PopupActionTypes } from "./popup.actions";

export interface IPopupState {
  selectedSecret: number | null;
  search: string;
  lastOpened: string | null;
  currentUrl: string | null;
}

export const initialState: IPopupState = {
  selectedSecret: null,
  search: "",
  lastOpened: null,
  currentUrl: null,
};

export function popupReducer(state = initialState, action: PopupActions): IPopupState {
  switch (action.type) {
    case PopupActionTypes.SET_SELECTED_SECRET:
      return {...state,
        selectedSecret: action.payload,
        lastOpened: new Date().toString(),
      };

    case PopupActionTypes.NULL_SELECTED_SECRET:
      return {...state,
        selectedSecret: initialState.selectedSecret,
        lastOpened: new Date().toString(),
      };

    case PopupActionTypes.SET_SEARCH:
      return {...state,
        search: action.payload,
        lastOpened: new Date().toString(),
      };

    case PopupActionTypes.SET_CURRENT_URL:
      return {...state,
        currentUrl: action.payload
      };

    default:
      return state;
  }
}

export const getSelectedSecret = (state: IPopupState) => state.selectedSecret;
export const getSearch = (state: IPopupState) => state.search;
export const getCurrentUrl = (state: IPopupState) => state.currentUrl;
