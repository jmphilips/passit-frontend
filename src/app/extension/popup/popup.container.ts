import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { map } from "rxjs/operators";
import { combineLatest, Observable } from "rxjs";

import { sanitizeUrl } from "@braintree/sanitize-url";
import * as fromRoot from "../../app.reducers";
import { filterOnSearch } from "../../secrets";
import * as secretActions from "../../secrets/secret.actions";
import { SecretService } from "../../secrets/secret.service";
import { copyToClipboard } from "../../utils";
import {
  SetCurrentUrlAction,
  SetSearchAction,
  SetSelectedSecretAction,
  NullSelectedSecretAction
} from "./popup.actions";
import { HotkeysService, Hotkey } from "angular2-hotkeys";
import { formFill } from "./autofill-compiled";
import { ISecret } from "passit-sdk-js/js/api.interfaces";

@Component({
  selector: "app-popup-container",
  template: `
    <app-popup
      [secrets]="secrets$ | async"
      [selectedSecret]="selectedSecret$ | async"
      [search]="search$ | async"
      [formFillMessage]="formFillMessage"
      [matchedSecrets]="matchedSecrets$ | async"
      (setSelected)="setSelected($event)"
      (closeSelected)="closeSelected()"
      (searchUpdate)="searchUpdate($event)"
      (openUrl)="openUrl($event)"
      (signIn)="signIn($event)"
      (onCopyUsername)="copyUsername($event)"
      (onCopyPassword)="copyPassword($event)"
      (onDetail)="openDetails($event)"
      (openFullApp)="openFullApp()"
    ></app-popup>
  `
})
export class PopupContainer implements OnInit {
  secrets$: Observable<ISecret[]>;
  search$ = this.store.select(fromRoot.getPopupSearch);
  selectedSecret$ = this.store.select(fromRoot.getPopupSelected);
  matchedSecrets$: Observable<ISecret[]>;
  formFillMessage = "";

  constructor(
    private store: Store<fromRoot.IState>,
    private secretService: SecretService,
    private _hotkeysService: HotkeysService
  ) {
    this._hotkeysService.add(
      new Hotkey(
        "ctrl+shift+l",
        (event: KeyboardEvent): boolean => {
          this.matchedSecrets$.subscribe(
            secrets => (secrets.length > 0 ? this.signIn(secrets[0]) : null)
          );
          return false;
        },
        ["input"]
      )
    );

    const getFiltered = (secrets: ISecret[], searchTerm: string) => {
      return filterOnSearch(secrets, searchTerm);
    };

    this.secrets$ = combineLatest(
      store.select(fromRoot.getSecrets),
      store.select(fromRoot.getPopupSearch)
    ).pipe(map(result => getFiltered(result[0], result[1])));

    this.matchedSecrets$ = combineLatest(
      this.secrets$,
      store.select(fromRoot.getPopupCurrentUrl)
    ).pipe(
      map(([secrets, url]) => {
        if (url) {
          url = this.extractRootDomain(url);
          return secrets.filter(secret => {
            if (secret.data.url !== undefined) {
              return this.extractRootDomain(secret.data.url!) === url;
            }
          });
        }
        return [];
      })
    );
  }

  ngOnInit() {
    // TODO make this not happen EVERY time
    this.getSecrets();
    this.checkUrlForMatch();
  }

  getSecrets() {
    this.store.dispatch(new secretActions.GetSecretsAction());
  }

  setSelected(selected: number) {
    this.store.dispatch(new SetSelectedSecretAction(selected));
    this.formFillMessage = "";
  }

  closeSelected() {
    this.store.dispatch(new NullSelectedSecretAction());
  }

  searchUpdate(term: string) {
    this.store.dispatch(new SetSearchAction(term));
  }

  openFullApp() {
    browser.tabs.create({
      url: "/index.html"
    });
    window.close();
  }

  copyUsername(secret: ISecret) {
    copyToClipboard(secret.data["username"]!);
  }

  copyPassword(secret: ISecret) {
    this.secretService.showSecret(secret).then(decrypted => {
      copyToClipboard(decrypted["password"]);
    });
  }

  openDetails(secret: ISecret) {
    browser.tabs.create({
      url: "/index.html#/list;id=" + secret.id
    });
    window.close();
  }

  signIn(secret: ISecret) {
    const username = secret.data["username"]!;
    this.secretService.showSecret(secret).then(decrypted => {
      const password = decrypted["password"];
      this.executeAutofillScript(username, password).then(() => {
        window.close();
      });
    });
  }

  checkUrlForMatch() {
    // TODO critical path for potential security audit
    // This url comes from the current tab domain - it could attempt to inject code.
    this.getActiveTab().then(tab => {
      if (tab.url !== undefined) {
        const url: string = sanitizeUrl(tab.url);
        this.store.dispatch(new SetCurrentUrlAction(url));
      }
    });
  }

  /** Run the autofill script on the current tab, requires using the activeTab permission.
   */
  executeAutofillScript(username: string, password: string) {
    return this.getActiveTab().then(tab => {
      // TODO critical path for potential security audit
      username = username.substring(0, 200);
      password = password.substring(0, 200);
      return browser.tabs
        .executeScript(tab.id, {
          code: formFill
        })
        .then(response => {
          browser.tabs.sendMessage(tab.id!, { username, password });
        });
    });
  }

  extractHostname(url: string) {
    let hostname;
    // find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1) {
      hostname = url.split("/")[2];
    } else {
      hostname = url.split("/")[0];
    }

    // find & remove port number
    hostname = hostname.split(":")[0];
    // find & remove "?"
    hostname = hostname.split("?")[0];

    return hostname;
  }

  extractRootDomain(url: string) {
    let domain = this.extractHostname(url);
    const splitArr = domain.split(".");
    const arrLen = splitArr.length;

    // extracting the root domain here
    // if there is a subdomain
    if (arrLen > 2) {
      domain = splitArr[arrLen - 2] + "." + splitArr[arrLen - 1];
      // check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
      if (
        splitArr[arrLen - 1].length === 2 &&
        splitArr[arrLen - 1].length === 2
      ) {
        // this is using a ccTLD
        domain = splitArr[arrLen - 3] + "." + domain;
      }
    }
    return domain;
  }

  /** Firefox and Chrome compatible way to get the active tab. tabs.getCurrent does not work in Firefox.
   * from https://stackoverflow.com/a/41943267
   */
  async getActiveTab() {
    const tabs = await browser.tabs.query({
      active: true,
      currentWindow: true
    });
    return tabs[0];
  }

  openUrl(secret: ISecret) {
    const url = secret.data["url"];
    if (url !== undefined) {
      return browser.tabs.create({ url: sanitizeUrl(url) });
    }
  }
}
