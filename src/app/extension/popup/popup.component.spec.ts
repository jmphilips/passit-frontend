import { HttpClientModule } from "@angular/common/http";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { InlineSVGModule } from "ng-inline-svg";

import { SharedModule } from "../../shared/";
import { PopupItemComponent } from "./popup-item";
import { PopupComponent } from "./popup.component";

describe("PopupComponent", () => {
  let component: PopupComponent;
  let fixture: ComponentFixture<PopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, InlineSVGModule.forRoot(), HttpClientModule],
      declarations: [PopupComponent, PopupItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupComponent);
    component = fixture.componentInstance;
    component.secrets = [];
    component.matchedSecrets = [];
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
