// tslint:disable:max-classes-per-file
import { Action } from "@ngrx/store";

export enum PopupActionTypes {
  SET_SELECTED_SECRET = "[Popup] Set Selected Secret",
  NULL_SELECTED_SECRET = "[Popup] Null Selected Secret",
  SET_SEARCH = "[Popup] Set Search",
  SET_CURRENT_URL = "[Popup] Set Current URL",
}

export class SetSelectedSecretAction implements Action {
  readonly type = PopupActionTypes.SET_SELECTED_SECRET;
  constructor(public payload: number) {}
}

export class NullSelectedSecretAction implements Action {
  readonly type = PopupActionTypes.NULL_SELECTED_SECRET;
}

export class SetSearchAction implements Action {
  readonly type = PopupActionTypes.SET_SEARCH;
  constructor(public payload: string) { }
}

export class SetCurrentUrlAction implements Action {
  readonly type = PopupActionTypes.SET_CURRENT_URL;
  constructor(public payload: string) {}
}

export type PopupActions
  = SetSelectedSecretAction
  | NullSelectedSecretAction
  | SetSearchAction
  | SetCurrentUrlAction;
