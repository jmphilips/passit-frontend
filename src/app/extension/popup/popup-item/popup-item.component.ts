import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ISecret } from "passit-sdk-js/js/api.interfaces";

@Component({
  selector: "app-popup-item",
  templateUrl: "./popup-item.component.html",
  styleUrls: ["./popup-item.component.scss"]
})
export class PopupItemComponent {
  passwordCopied = false;
  usernameCopied = false;
  @Input() secret: ISecret;
  @Input() formFillMessage: string;
  @Input() isMatched = false;
  @Input() isSelected = false;
  @Output() openUrl = new EventEmitter();
  @Output() autofill = new EventEmitter();
  @Output() onCopyUsername = new EventEmitter();
  @Output() onCopyPassword = new EventEmitter();
  @Output() onDetail = new EventEmitter();
  @Output() setSelected = new EventEmitter<number>();
  @Output() closeSelected = new EventEmitter();

  resetCopyState() {
    this.usernameCopied = false;
    this.passwordCopied = false;
  }

  clickAutofill(event: Event) {
    event.stopPropagation();
    this.autofill.emit();
  }

  clickUsername(event: Event) {
    event.stopPropagation();
    this.onCopyUsername.emit();
    this.usernameCopied = true;
    setTimeout(() => this.resetCopyState(), 1000);
  }

  clickPassword(event: Event) {
    event.stopPropagation();
    if (this.secret.secret_through_set[0].data.password) {
      this.onCopyPassword.emit();
      this.passwordCopied = true;
      setTimeout(() => this.resetCopyState(), 1000);
    }
  }
}
