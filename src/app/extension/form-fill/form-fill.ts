// tslint:disable:variable-name
// tslint:disable:no-bitwise
// tslint:disable:no-var-keyword
// tslint:disable:prefer-const
const isVisible = (element: HTMLElement) => {
  return element.offsetWidth > 0 || element.offsetHeight > 0;
};

const LARGE_SCORE_VALUE = 1000;

interface IFieldDict {
  name: string;
  id: string;
  class: string;
  type: string;
  maxlength?: number;
  value: string;
  pointer: HTMLElement;
  itemNo: number;
  emptyPasswordPermitted?: boolean;
}

interface IFormDict {
  usernameField: IFieldDict;
  passwordField: IFieldDict;
  submitField: IFieldDict;
  allFields: IFieldDict[];
  formDict: IFieldDict;
  id: string;
  loginUrl?: string;
}

const createFieldDict = (fields: HTMLElement[]) => {
  const rval: IFieldDict[] = [];
  for (let i = 0; i < fields.length; ++i) {
    const field = fields[i];

    if (field.tagName === "BUTTON") {
      const button: HTMLButtonElement = field as HTMLButtonElement;
      rval.push({
        name: button.name,
        id: button.id,
        class: button.className,
        value: button.textContent!,
        type: "button",
        pointer: field,
        itemNo: i
      });
    } else if (field.tagName === "A") {
      const anchor: HTMLAnchorElement = field as HTMLAnchorElement;
      rval.push({
        name: anchor.name || field.className,
        id: anchor.id,
        class: anchor.className,
        type: "a",
        value: field.textContent!,
        pointer: field,
        itemNo: i
      });
    } else {
      const input: HTMLInputElement = field as HTMLInputElement;
      rval.push({
        name: input.name,
        id: input.id,
        class: input.className,
        value: input.type === "image" ? input.alt : input.value,
        type: input.type || "text",
        maxlength: input.maxLength > -1 ? input.maxLength : 10,
        pointer: field,
        itemNo: i
      });
    }
  }
  return rval;
};

const getMaximumElement = (
  elements: any,
  scoreFunc: (IFieldDict: any) => number
) => {
  if (elements) {
    let maxScore = -1;
    let maxElement: any = null;

    for (const element of elements) {
      const score = scoreFunc(element);
      if (score > maxScore) {
        maxScore = score;
        maxElement = element;
      }
    }
    return maxElement;
  }
  return null;
};

/* Score a username input field.  Field type is the most important criteria.
 * Having a non-empty value is useful to determine the username when saving a
 * form.
 */
const usernameScoreFunc = (a: IFieldDict, passwordFieldHint?: IFieldDict) => {
  let score = 0;

  if (a.type === "text" || a.type === "email") {
    score += 2;
  } else {
    score -= LARGE_SCORE_VALUE;
  }
  if (passwordFieldHint) {
    // as of jquery 1.3.2, items are returned in document order, this
    // distance will tell us the number of matching elements between
    // the two items.
    const distance = passwordFieldHint.itemNo - a.itemNo;
    if (distance > 0) {
      score += 1.0 / distance;
    }
  }

  return score;
};

const guessUsernameField = (
  elements: IFieldDict[],
  passwordFieldHint: IFieldDict
) => {
  const usernameField = getMaximumElement(elements, a =>
    usernameScoreFunc(a, passwordFieldHint)
  );
  // Enforce a minimum threshold to prevent really bad guesses.
  if (usernameField !== null && usernameScoreFunc(usernameField) > 0) {
    return usernameField;
  } else {
    return null;
  }
};

/* Score a password input field.  Field type is the most important criteria.
 * Having a non-empty value is useful to determine the username when saving a
 * form.  Non-password fields are heavily penalized.
 */
const passwordScoreFunc = (a: IFieldDict) => {
  let score = a.value ? 1 : 0;
  const name = a.name ? a.name.toLowerCase() : "";
  if (a.type === "password") {
    // catch credit card CCV fields
    if (a.maxlength! <= 4) {
      score -= LARGE_SCORE_VALUE;
    } else if (name.indexOf("creditcard") !== -1) {
      score -= LARGE_SCORE_VALUE;
    } else {
      score += 2;
    }
  } else {
    score -= LARGE_SCORE_VALUE;
  }
  return score;
};

const guessPasswordField = (elements: IFieldDict[]): IFieldDict | null => {
  const passwordField = getMaximumElement(elements, passwordScoreFunc);

  if (passwordField !== null && passwordScoreFunc(passwordField) > 0) {
    return passwordField;
  } else {
    return null;
  }
};

/* we allow image submit buttons for SUBMITTING once we have chosen
 * a form, but ignore them for RANKING purposes.
 */
const guessSubmitField = (
  elements: IFieldDict[],
  allowImageButtons: boolean,
  $preferAfterThisField: any
) => {
  const submitScoreFunc = (a: any) => {
    if (a.type === "text" || a.type === "password") {
      return -LARGE_SCORE_VALUE;
    }
    let score = a.type === "submit" || a.type === "button" ? 1 : 0;
    // in some cases (when submitting the form) we want to match image
    // buttons
    score += a.type === "image" && allowImageButtons ? 0.75 : 0;
    score += a.type === "a" ? 0.25 : 0;
    const value = a.value ? a.value.toLowerCase() : "";
    if (value.indexOf("forgot") !== -1) {
      score -= LARGE_SCORE_VALUE;
    }
    // prefer exact matches.
    // this helps avoid things like "forgot login"
    if (
      // TODO: internationalize
      value === "sign in" ||
      value === "log in" ||
      value === "log on" ||
      value === "submit" ||
      value === "login" ||
      value === "go"
    ) {
      score += 2;
    } else if (
      value.indexOf("sign in") !== -1 ||
      value.indexOf("log in") !== -1 ||
      value.indexOf("log on") !== -1 ||
      value.indexOf("submit") !== -1 ||
      value.indexOf("login") !== -1 ||
      value.indexOf("go") !== -1
    ) {
      score += 1;
    }

    if ($preferAfterThisField) {
      const BITMASK = 4; // Node.DOCUMENT_POSITION_FOLLOWING is defined as 4 but not in node.
      const isAfter =
        $preferAfterThisField.compareDocumentPosition(a.pointer) & BITMASK;
      score += isAfter ? 1 : -1;
    }

    return score;
  };
  const submitField = getMaximumElement(elements, submitScoreFunc);
  if (submitField !== null && submitScoreFunc(submitField) > 0) {
    return submitField;
  } else {
    return null;
  }
};

/** Returns a login form dict if input form is a login form, or null otherwise.
 * Setting requireFieldVisibility only considers visible form fields when
 * looking for username/password/submit fields (default: true).
 */
const getLoginForm = (form: HTMLFormElement, requireFieldVisibility = true) => {
  const fieldNodes = form.querySelectorAll(
    "input:not([type=hidden]),button:not([type=hidden]),a"
  );
  let fields = Array.prototype.slice.call(fieldNodes, 0);
  if (requireFieldVisibility) {
    fields = Array.prototype.filter.call(fields, (field: any) =>
      isVisible(field)
    );
  }
  const fieldsRecord = createFieldDict(fields);
  const passwordField = guessPasswordField(fieldsRecord);
  let usernameField: IFieldDict | null = null;
  let goAhead = !!passwordField;
  if (passwordField) {
    usernameField = guessUsernameField(fieldsRecord, passwordField);
  }

  goAhead = goAhead && usernameField !== null;
  if (goAhead) {
    const submitField = guessSubmitField(
      fieldsRecord,
      true,
      usernameField && usernameField.pointer
    );
    const fieldDict = createFieldDict([
      (passwordField ? (passwordField as any) : usernameField).pointer.closest(
        "form"
      )
    ]);
    // we need to find a form.
    if (!fieldDict) {
      return null;
    }
    return {
      usernameField,
      passwordField,
      submitField,
      allFields: fieldsRecord,
      formDict: fieldDict[0],
      id: form.id
    };
  } else {
    return null;
  }
};

const LOGIN_BUTTON_LABELS = ["login", "log in", "log on", "signin", "sign in"];
const SIGNUP_BUTTON_LABELS = [
  "signup",
  "sign up",
  "join",
  "create",
  "register",
  "start",
  "free",
  "trial"
];

/* Score the submit button as */
const loginSubmitButtonScoreFunc = (field: any) => {
  let submitScore = 0;

  if (field && field.value) {
    const label = field.value.toLowerCase();
    let i;
    let s;

    for (i = 0; i < LOGIN_BUTTON_LABELS.length; ++i) {
      s = LOGIN_BUTTON_LABELS[i];
      if (label.indexOf(s) !== -1) {
        submitScore += 2;
      }
    }
    for (i = 0; i < SIGNUP_BUTTON_LABELS.length; ++i) {
      s = SIGNUP_BUTTON_LABELS[i];
      if (label.indexOf(s) !== -1) {
        submitScore -= LARGE_SCORE_VALUE;
      }
    }
  }

  return submitScore;
};

const LOGIN_FORM_ID_STRINGS = ["login", "signin"];
const SIGNUP_FORM_ID_STRINGS = ["signup", "regist"];

const loginIdScoreFunc = (formId: string) => {
  let idScore = 0;

  if (formId) {
    const id = formId.toLowerCase();
    let i;

    for (i = 0; i < LOGIN_FORM_ID_STRINGS.length; ++i) {
      const s = LOGIN_FORM_ID_STRINGS[i];
      if (id.indexOf(s) !== -1) {
        idScore += 1;
      }
    }
    // sometimes forms have both signup and signin.  we should not punish those forms.
    if (idScore === 0) {
      for (i = 0; i < SIGNUP_FORM_ID_STRINGS.length; ++i) {
        const s = SIGNUP_FORM_ID_STRINGS[i];
        if (id.indexOf(s) !== -1) {
          idScore -= LARGE_SCORE_VALUE;
        }
      }
    }
  }
  return idScore;
};

const countFieldsOfType = (fields: any[], type: any) => {
  let count = 0;
  for (const field of fields) {
    if (field.type === type) {
      ++count;
    }
  }
  return count;
};

const loginFormScoreFunc = (formDict: IFormDict) => {
  if (!formDict) {
    return 0;
  }

  let score = 0;
  if (formDict.usernameField) {
    score += usernameScoreFunc(formDict.usernameField);
  }
  if (
    !formDict.passwordField &&
    formDict.usernameField.emptyPasswordPermitted
  ) {
    /* ? */
  } else {
    score += passwordScoreFunc(formDict.passwordField as any);
  }
  score += loginSubmitButtonScoreFunc(formDict.submitField as any);
  score += loginIdScoreFunc(formDict.id);
  // Prevents matches for signup and change password forms.
  if (countFieldsOfType(formDict.allFields, "password") > 1) {
    score -= LARGE_SCORE_VALUE;
  }
  return score;
};

/* Finds best match for a login form in the current document, or null if no
 * login form is found.
 */
const guessLoginForm = (hints?: any) => {
  const scoringFunction = loginFormScoreFunc;
  const formSelection: HTMLFormElement[] = Array.prototype.slice.call(
    document.querySelectorAll("form")
  );
  const forms = formSelection.map(form => {
    return getLoginForm(form);
  });
  const loginForm: IFormDict = getMaximumElement(forms, scoringFunction);
  if (loginForm && scoringFunction(loginForm) < 0) {
    return null;
  }
  return loginForm;
};

const getCanonicalHost = (fullUrl: string) => {
  return "http://stub.com";
};

const fillLoginForm = (formData: IFormDict) => {
  const formDomain = getCanonicalHost(formData.loginUrl!);
  const pageDomain = getCanonicalHost(document.URL);

  if (formDomain !== pageDomain) {
    throw new Error(
      "Domain mismatch: " + pageDomain + ", expected: " + formDomain
    );
  }

  const un = formData.usernameField ? formData.usernameField.value : null;
  const pw = formData.passwordField ? formData.passwordField.value : null;

  // forms sometimes load strangely.
  // TODO: Which ones? What does that mean exactly?
  // Lastpass can cause conflicts: previously we filled the form then delayed submit()
  // lastpass would fill the form in the middle, causing a login for the wrong account.
  setTimeout(() => {
    // Find the form with the username / password
    const sendKeyEvents = (inputId: any) => {
      // TODO: figure out how to enable this without horrible performance problems
      let event = new KeyboardEvent("keydown");
      // event.key = 13;
      document.querySelector("#" + inputId)!.dispatchEvent(event);
      event = new KeyboardEvent("keypress");
      // event.key = 13;
      document.querySelector("#" + inputId)!.dispatchEvent(event);
      event = new KeyboardEvent("keyup");
      // event.key = 13;
      document.querySelector("#" + inputId)!.dispatchEvent(event);
    };

    const $user_input = (formData.usernameField
      ? formData.usernameField.pointer
      : formData.usernameField) as HTMLInputElement;

    if ($user_input) {
      var $user_form = $user_input.closest("form");
      $user_input.value = un!;
      let userInputId = $user_input.id;
      if (!userInputId) {
        userInputId = "MITRO____184379378465893";
        $user_input.setAttribute("id", userInputId);
      }
      setTimeout(() => {
        sendKeyEvents(userInputId);
      }, 10);
    }
    if (formData.passwordField) {
      const $pass_input = formData.passwordField.pointer as HTMLInputElement;
      // assert($pass_input.attr("type") === "password");
      var $pass_form = $pass_input.closest("form");
      $pass_input.value = pw!;
      // HACK
      // helper.preventAutoFill($pass_input, $pass_form);

      // simulate keypresses
      let passInputId = $pass_input.getAttribute("id");
      if (!passInputId) {
        passInputId = "MITRO____184379378465894";
        $pass_input.setAttribute("id", passInputId);
      }

      setTimeout(() => {
        sendKeyEvents(passInputId);
      }, 20);
      // This call will disable autocomplete and the browser won't offer to save
      // the password on the browser's secure area
      // Tested in Chrome and Firefox
      $pass_form![0].setAttribute("autocomplete", "off");
      if ($user_input) {
        $user_input.setAttribute("autocomplete", "off");
      }
      $pass_input.setAttribute("autocomplete", "off");
    }

    // Simulate implicit form submission (e.g. pressing enter)
    // tslint:disable-next-line:max-line-length
    // http://www.whatwg.org/specs/web-apps/current-work/multipage/association-of-controls-and-forms.html#implicit-submission
    // Click the first submit button. Submit buttons are:
    // - <input type="submit">
    // - <input type="image">
    // - <button> (with type="submit" or no type)
    // selecting buttons without type attribute is tricky; do that in a separate query
    setTimeout(() => {
      const submit_button = formData.submitField;
      if (submit_button) {
        try {
          // sometimes the code runs validators and the submit button is disabled
        } catch (e) {
          submit_button.pointer.removeAttribute("disabled");
        }
        submit_button.pointer.click();
      } else {
        // this may work for forms without submit buttons (e.g. buttons with onclick)
        try {
          // if form has input[name="submit"] it shadows submit() method causing TypeError
          $pass_form![0].submit();
        } catch (e) {
          if (e instanceof TypeError) {
            try {
              // console.log('form.submit not a method; trying to click');
              $pass_form![0].submit.click();
            } catch (e2) {
              // console.log('trying to click on the submit button');
              const $submit_thing = $user_form!.querySelector(
                ":submit"
              ) as HTMLElement;
              $submit_thing.click();
            }
          }
        }
      }
    }, 300);
  }, 200);
};

interface IMessage {
  username: string;
  password: string;
}

const guessAndFillLoginForm = (message: IMessage) => {
  let formData: any = {
    clientData: { loginUrl: document.URL, username: message.username },
    criticalData: { password: message.password }
  };
  const loginForm = guessLoginForm(formData);
  if (!loginForm) {
    return false;
  }

  loginForm.loginUrl = formData.clientData.loginUrl;
  if (loginForm.usernameField) {
    loginForm.usernameField.value = formData.clientData.username;
  }
  if (loginForm.passwordField) {
    loginForm.passwordField.value = formData.criticalData.password;
  }

  fillLoginForm(loginForm);
  return true;
};

// Uncomment for real usage, leave out for tests
// Google Chrome usually supports "browser" but on the executed script it does not.
// declare var chrome: any;
// If Chrome
// if (typeof browser === "undefined" && typeof chrome !== "undefined") {
//   chrome.runtime.onMessage.addListener((message: IMessage) => {
//     guessAndFillLoginForm(message);
//   });
//   // If anything standards compliant (browser)
// } else if (typeof browser !== "undefined") {
//   browser.runtime.onMessage.addListener((message: any) => {
//     guessAndFillLoginForm(message);
//   });
// }

// Comment for real usage, but needed in test.
export { guessAndFillLoginForm, guessLoginForm };
