import { SecretFormContainer } from "./secret-form.container";
import {
  ComponentFixture,
  async,
  TestBed,
  fakeAsync,
  tick
} from "@angular/core/testing";
import { NgrxFormsModule } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule } from "@ngrx/store";

import * as fromList from "../list.reducer";
import * as fromRoot from "../../app.reducers";
import { SecretFormComponent } from "./secret-form.component";
import { ClipboardModule } from "ngx-clipboard";
import { SelectModule } from "ng-select";
import { groupReducer } from "../../group/group.reducer";
import { GeneratorService } from "../../secrets";
import { EffectsModule } from "@ngrx/effects";
import { SecretFormEffects } from "./secret-form.effects";
import { SecretService } from "../../secrets/secret.service";

class FakeService {}

describe("Secret Form", () => {
  let component: SecretFormContainer;
  let fixture: ComponentFixture<SecretFormContainer>;
  let generatorService: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgrxFormsModule,
        InlineSVGModule.forRoot(),
        ClipboardModule,
        SelectModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("list", fromList.reducers),
        StoreModule.forFeature("group", groupReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([SecretFormEffects])
      ],
      declarations: [SecretFormContainer, SecretFormComponent],
      providers: [
        {
          provide: GeneratorService,
          useValue: jasmine.createSpyObj("generatorService", [
            "generatePassword"
          ])
        },
        { provide: SecretService, useClass: FakeService }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretFormContainer);
    component = fixture.componentInstance;
    generatorService = TestBed.get(GeneratorService);
  });

  it("should exist", () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it("should require a name", () => {
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#submit-button").click();
    fixture.detectChanges();
    expect(fixture.nativeElement.innerText.indexOf("Required") !== -1).toBe(
      true
    );
  });

  it(
    "should set a password",
    fakeAsync(() => {
      const fakeRandomPassword = "fj3092fn302wign3q4it";
      generatorService.generatePassword.and.returnValue(
        Promise.resolve(fakeRandomPassword)
      );
      fixture.detectChanges();
      fixture.debugElement.nativeElement
        .querySelector("#generatePasswordButton")
        .click();
      tick(100);
      fixture.detectChanges();
      const passwordInput = fixture.debugElement.nativeElement.querySelector(
        "#Secret\\ Form\\.password"
      );
      expect(passwordInput.value).toBe(fakeRandomPassword);
    })
  );
});
