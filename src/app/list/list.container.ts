import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { combineLatest, Observable } from "rxjs";
import { map } from "rxjs/operators";

import * as fromRoot from "../app.reducers";
import { GetGroupsAction } from "../group/group.actions";
import * as fromList from "./list.reducer";

import { ResetFormContainer } from "../form/reset-form.container";
import * as list from "../list/list.actions";
import { filterOnSearch } from "../secrets";
import * as secretActions from "../secrets/secret.actions";

import * as api from "passit-sdk-js/js/api.interfaces";

@Component({
  selector: "secret-list-container",
  template: `
    <secret-list-component
      [secrets]="secrets$ | async"
      [totalSecretsCount]="totalSecretsCount$ | async"
      [secretManaged]="secretManaged$ | async"
      [showCreate]="showCreate$ | async"
      [secretIdFromUrl]="secretIdFromUrl"
      [searchText]="searchText$ | async"
      (secretWasSelected)="secretWasSelected($event)"
      (hideAddSecretForm)="hideAddSecretForm()"
      (showAddSecretForm)="showAddSecretForm()"
      (searchUpdate)="searchUpdate($event)"
      [firstTimeLoadingComplete]="firstTimeLoadingComplete$ | async"
    ></secret-list-component>

  `
})
export class SecretListContainer extends ResetFormContainer implements OnInit {
  showCreate$ = this.store.select(fromList.getListShowCreate);
  secrets$: Observable<api.ISecret[]>;
  secretManagedObject$: Observable<api.ISecret | undefined>;
  secretManaged: number | null;
  secretManaged$ = this.store.select(fromList.getListSecretManaged);
  totalSecretsCount$ = this.store.select(fromRoot.getSecretsCount);
  searchText$ = this.store.select(fromList.getSearchText);
  secretIdFromUrl: number | null = null;
  firstTimeLoadingComplete$ = this.store.select(
    fromList.getFirstTimeLoadingComplete
  );

  constructor(
    public store: Store<fromRoot.IState>,
    private route: ActivatedRoute
  ) {
    super(store);
    store
      .select(fromList.getListSecretManaged)
      .subscribe(secretId => (this.secretManaged = secretId));
    this.secrets$ = combineLatest(
      this.store.select(fromRoot.getSortedSecrets),
      this.searchText$,
      this.secretManaged$
    ).pipe(map(result => filterOnSearch(result[0], result[1], result[2]!)));

    // Mobile app needs this.
    this.secretManagedObject$ = combineLatest(
      this.secrets$,
      this.secretManaged$
    ).pipe(
      map(([secrets, secretManagedId]) =>
        secrets.find(secret => secret.id === secretManagedId)
      )
    );
  }

  public ngOnInit() {
    super.ngOnInit();
    this.getSecrets();
    // web ext popup uses this
    this.route.params.subscribe(params => {
      if (params["id"]) {
        const secretId = Number(params["id"]);
        this.secretIdFromUrl = secretId;
        this.store.dispatch(new list.SetManagedSecret(secretId));
      }
    });
    this.getGroups();
  }

  /** Trigger refresh of secrets data */
  public getSecrets() {
    this.store.dispatch(new secretActions.GetSecretsAction());
  }

  public getGroups() {
    this.store.dispatch(new GetGroupsAction());
  }

  /*
  * receive secret from child when secret clicked
  */
  public secretWasSelected(secretId: number) {
    if (secretId === this.secretManaged) {
      this.store.dispatch(new list.ClearManagedSecret());
    } else {
      this.store.dispatch(new list.SetManagedSecret(secretId));
    }
  }

  public searchUpdate(term: string) {
    this.store.dispatch(new list.SetSearchText(term));
  }

  showAddSecretForm() {
    this.store.dispatch(new list.ShowCreate());
  }

  hideAddSecretForm() {
    this.store.dispatch(new list.HideCreate());
  }
}
