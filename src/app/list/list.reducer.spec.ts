import { ISecret } from "passit-sdk-js/js/api.interfaces";
import * as SecretActions from "../secrets/secret.actions";
import * as ListActions from "./list.actions";
import * as fromList from "./list.reducer";

describe("ListReducer", () => {
  describe("undefined action", () => {
    it("should return the default state", () => {
      const action = {} as any;

      const result = fromList.listReducer(undefined, action);
      expect(result).toEqual(fromList.initialListState);
    });
  });

  describe("CLEAR_MANAGED_SECRET", () => {
    it("should set secretManaged to null", () => {
      const createAction = new ListActions.ClearManagedSecret();
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result).toEqual(fromList.initialListState);
    });
  });

  describe("TOGGLE_MANAGED_SECRET", () => {
    it("should set managed secret", () => {
      const expectedResult = {
        searchText: "",
        secretManaged: 1,
        showCreate: false,
        secretIsUpdating: false,
        secretIsUpdated: false,
        firstTimeLoadingComplete: false
      };

      const createAction = new ListActions.SetManagedSecret(
        expectedResult.secretManaged
      );
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result).toEqual(expectedResult);
    });
  });

  describe("SET_SEARCH_TEXT", () => {
    it("should set search text", () => {
      const expectedResult = {
        searchText: "test",
        secretManaged: null,
        showCreate: false,
        firstTimeLoadingComplete: false
      };

      const createAction = new ListActions.SetSearchText(
        expectedResult.searchText
      );
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result).toEqual(expectedResult);
    });
  });

  describe("SHOW_CREATE", () => {
    it("should set showCreate to true", () => {
      const expectedResult = {
        searchText: "",
        secretManaged: null,
        showCreate: true,
        secretIsUpdating: false,
        secretIsUpdated: false,
        firstTimeLoadingComplete: false
      };

      const createAction = new ListActions.ShowCreate();
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result).toEqual(expectedResult);
    });
  });

  describe("HIDE_CREATE", () => {
    it("should set showCreate to false", () => {
      const createAction = new ListActions.HideCreate();
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result).toEqual(fromList.initialListState);
    });
  });

  describe("REPLACE_SECRET_SUCCESS indicates a secret is finished saving.", () => {
    it("should set secretIsUpdating to false and secretIsUpdating to true", () => {
      const replacedSecret: ISecret = {
        name: "test",
        type: "test",
        id: 1,
        data: {},
        secret_through_set: []
      };
      const startState: fromList.IListState = {
        ...fromList.initialListState,
        secretManaged: 1
      };
      const createAction = new SecretActions.ReplaceSecretSuccessAction(
        replacedSecret
      );
      const result = fromList.listReducer(startState, createAction);

      // This should remain as we don't want to close a secret on save.
      expect(result.secretManaged).toEqual(1);
    });
  });
});
