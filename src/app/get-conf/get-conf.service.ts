import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { NgPassitSDK } from "../ngsdk/sdk";

import { IConfResponse } from "./conf.interfaces";
@Injectable()
export class GetConfService {
  constructor(private sdk: NgPassitSDK) {}

  /** Get configuration from server and save it to local state
   */
  public getConf(): Observable<IConfResponse> {
    return Observable.fromPromise(this.sdk.get_conf()) as Observable<
      IConfResponse
    >;
  }
}
