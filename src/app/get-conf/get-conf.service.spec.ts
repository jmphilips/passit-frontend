import { TestBed } from "@angular/core/testing";
import { StoreModule } from "@ngrx/store";

import { NgPassitSDK } from "../ngsdk/sdk";

import { reducers } from "../app.reducers";
import { GetConfService } from "./get-conf.service";

class FakeSDK {
  public get_conf(): Promise<{}> {
    return new Promise((resolve, reject) => {
      resolve({
        IS_DEMO: true
      });
    });
  }
}

describe("Get Conf Service", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot(reducers)],
      providers: [
        NgPassitSDK,
        GetConfService,
        { provide: NgPassitSDK, useClass: FakeSDK }
      ]
    });
  });

  //    it("get conf from server and save it to local state",
  //     inject([GetConfService, Store],
  //     fakeAsync((service: GetConfService, store: Store<IState>
  //   ) => {
  //     service.rehydrate().then(() => {
  //       store.select(getConfState).subscribe((confState) => {
  //         expect(confState.isDemo).toBe(true);
  //         expect(confState.timestamp).toBeTruthy();
  //       });
  //     });
  //   })));
});
