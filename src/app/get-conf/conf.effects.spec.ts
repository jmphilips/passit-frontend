import { TestBed } from "@angular/core/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { Store, StoreModule } from "@ngrx/store";
import { Observable } from "rxjs";
import { IState, reducers } from "../app.reducers";
import { GetConfAction, SetConfAction } from "./conf.actions";
import { GetConfEffects } from "./conf.effects";
import { IConfResponse } from "./conf.interfaces";
import { initial } from "./conf.reducer";
import { GetConfService } from "./get-conf.service";

import { cold, hot } from "jasmine-marbles";
import { HandleAPIErrorAction } from "../account/account.actions";

const junkState: any = null;

const mockStore: IState = {
  conf: initial,
  contacts: junkState,
  importer: junkState,
  popup: junkState,
  router: junkState,
  secrets: junkState
};

const fakeResp: IConfResponse = {
  IS_DEMO: false,
  IS_PRIVATE_ORG_MODE: false
};

describe("Conf Effects", () => {
  let effects: GetConfEffects;
  let actions: Observable<any>;
  let service: any;
  let store: Store<IState>;

  const prepTest = (initialState?: IState) => {
    if (!initialState) {
      initialState = mockStore;
    }
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot(reducers, { initialState })],
      providers: [
        GetConfEffects,
        provideMockActions(() => actions),
        {
          provide: GetConfService,
          useValue: jasmine.createSpyObj("GetConfService", ["getConf"])
        }
      ]
    });

    effects = TestBed.get(GetConfEffects);
    service = TestBed.get(GetConfService);
    store = TestBed.get(Store);
  };

  it("should call getConf if timestamp is null", () => {
    prepTest();
    service.getConf.and.returnValue(Observable.of(fakeResp));
    actions = hot("a", { a: new GetConfAction() });
    const expected = cold("b", { b: new SetConfAction(fakeResp) });
    expect(effects.getConf$).toBeObservable(expected);
    expect(service.getConf).toHaveBeenCalled();
  });

  it("should not call getConf if less than 24 hours has passed", () => {
    prepTest();
    // Set the store to have run just now
    store.dispatch(new SetConfAction(fakeResp));

    service.getConf.and.returnValue(Observable.of(fakeResp));
    actions = hot("-a---", { a: new GetConfAction() });
    const expected = cold("-");
    expect(effects.getConf$).toBeObservable(expected);
    expect(service.getConf).not.toHaveBeenCalled();
  });

  it("should call getConf if more than 24 hours has passed", () => {
    const oldDate = "Sat Oct 21 2016 14:08:32 GMT-0400 (EDT)";
    const oldTimeMockStore = Object.assign({}, mockStore, {
      conf: {
        isDemo: false,
        isPrivateOrgMode: false,
        timestamp: oldDate
      }
    });
    prepTest(oldTimeMockStore);

    service.getConf.and.returnValue(Observable.of(fakeResp));
    actions = hot("a", { a: new GetConfAction() });
    const expected = cold("b", { b: new SetConfAction(fakeResp) });
    expect(effects.getConf$).toBeObservable(expected);
    expect(service.getConf).toHaveBeenCalled();
  });

  it("should handle GetConfAction errors", () => {
    prepTest();
    actions = hot("a", { a: new GetConfAction() });
    const response = cold("-#", {});
    service.getConf.and.returnValue(response);
    const expected = cold("-b", { b: new HandleAPIErrorAction("error") });
    expect(effects.getConf$).toBeObservable(expected);
  });
});
