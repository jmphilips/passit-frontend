import { Actions, ConfTypes } from "./conf.actions";

export interface IConfState {
  isDemo: boolean;
  isPrivateOrgMode: boolean;
  /** Is operating in web extension popup */
  isPopup: boolean;
  timestamp: string | null;
}

export const initial: IConfState = {
  isDemo: false,
  isPrivateOrgMode: false,
  isPopup: false,
  timestamp: null
};

export function reducer(state = initial, action: Actions): IConfState {
  switch (action.type) {
    case ConfTypes.SET_CONF: {
      return {
        ...state,
        isDemo: action.payload.IS_DEMO,
        isPrivateOrgMode: action.payload.IS_PRIVATE_ORG_MODE,
        timestamp: new Date().toString()
      };
    }

    case ConfTypes.SET_IS_POPUP: {
      return {
        ...state,
        isPopup: true
      };
    }

    default: {
      return state;
    }
  }
}

export const getIsDemo = (state: IConfState) => state.isDemo;
export const getIsPrivateOrgMode = (state: IConfState) =>
  state.isPrivateOrgMode;
export const getTimestamp = (state: IConfState) => state.timestamp;
export const getIsPopup = (state: IConfState) => state.isPopup;
