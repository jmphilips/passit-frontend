export interface IConfResponse {
  IS_DEMO: boolean;
  IS_PRIVATE_ORG_MODE: boolean;
}
