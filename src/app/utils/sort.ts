interface NamedThing {
  name: string;
}

export const sortList = <T extends NamedThing>(array: T[]): T[] => {
  // Workaround for bad ngrx localstorage scheme
  // https://gitlab.com/passit/passit-frontend/issues/145
  if (!array) {
    return [];
  }
  return array.sort((a, b) => {
    return a.name.toLowerCase() < b.name.toLowerCase()
      ? -1
      : a.name.toLowerCase() > b.name.toLowerCase()
        ? 1
        : 0;
  });
};
