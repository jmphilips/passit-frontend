import { ISecret } from "passit-sdk-js/js/api.interfaces";
import { sortList } from "../utils/sort";
import { SecretActionTypes, SecretActionsUnion } from "./secret.actions";

export interface ISecretState {
  secrets: ISecret[];
}

export const initialState: ISecretState = {
  secrets: []
};

export function secretReducer(
  state = initialState,
  action: SecretActionsUnion
): ISecretState {
  switch (action.type) {
    case SecretActionTypes.REMOVE_SECRET:
      const secrets = state.secrets.filter(s => {
        return s.id !== action.payload;
      });
      return Object.assign({}, state, {
        secrets
      });

    case SecretActionTypes.REPLACE_SECRET_SUCCESS:
      const index = state.secrets.findIndex(s => {
        return s.id === action.payload.id;
      });
      const updatedSecrets = state.secrets.slice();
      updatedSecrets[index] = action.payload;
      return Object.assign({}, state, {
        secrets: updatedSecrets
      });

    case SecretActionTypes.REPLACE_SECRET:
      return Object.assign({}, state, {
        ...state.secrets
      });

    case SecretActionTypes.SET_SECRETS:
      return Object.assign({}, state, {
        secrets: action.payload
      });

    default:
      return state;
  }
}

export const getSecrets = (state: ISecretState) => state.secrets;
export const getSortedSecrets = (state: ISecretState) =>
  sortList(state.secrets);
