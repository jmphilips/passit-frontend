FROM node:8-alpine

RUN mkdir /dist
WORKDIR /dist

COPY package.json /dist/package.json
COPY yarn.lock /dist/yarn.lock
RUN yarn install --frozen-lockfile && yarn cache clean

VOLUME /dist/node_modules
ENV PATH /dist/node_modules/.bin:$PATH
COPY . /dist/

ENTRYPOINT ["yarn", "run"]
