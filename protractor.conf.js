// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

const { DOCKER, FIREFOX, EXTENSION } = process.env;

if (FIREFOX && EXTENSION) {
  throw new Error('Can\'t test Firefox with extension at the moment. https://gitlab.com/passit/passit-frontend/issues/13')
}

exports.config = {
  allScriptsTimeout: 39000,  // 39s to make it easier to identify
  specs: [
    './e2e/**/*.e2e-spec.ts'
  ],
  capabilities: FIREFOX ? {
    'browserName': 'firefox',
  } : {
    'browserName': 'chrome',
    'chromeOptions': {
      'args': EXTENSION ? ['--no-sandbox', `--load-extension=/dist/dist/`] : ['--no-sandbox']
    }
  },
  directConnect: !DOCKER,
  // chrome extension ID remain constant if the path to the ext remains constant
  // https://stackoverflow.com/a/26058672
  baseUrl: EXTENSION ? 'chrome-extension://hncijbkokpgjokbcdmcelmfndciahmkf/index.html#/' : (DOCKER ? 'http://web:4200/' : 'http://localhost:4200/'),
  seleniumAddress: DOCKER ? `http://selenium-${FIREFOX ? 'firefox' : 'chrome'}:4444/wd/hub/` : undefined,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 50000,
    print: function() {}
  },
  useAllAngular2AppRoots: true,
  beforeLaunch: function() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  onPrepare() {
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    return global.browser.getProcessedConfig().then(function (config) {});
  },
  plugins: [{
    package: 'protractor-screenshoter-plugin',
    screenshotOnExpect: 'failure',
    screenshotOnSpec: 'failure',
  }],
  SELENIUM_PROMISE_MANAGER: false
};
