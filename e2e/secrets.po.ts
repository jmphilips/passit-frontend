// tslint:disable:semicolon
import { by, element, Key, ProtractorBrowser } from "protractor";

export class SecretsPage {
  getFormElem = (testBrowser: ProtractorBrowser) =>
    testBrowser.element(by.className("secret__details"));

  getSearchElem = () => element(by.id("search"));
  navigateTo = async (testBrowser: ProtractorBrowser) =>
    await testBrowser.get(testBrowser.baseUrl + "/list");
  selectAddNewPassword = async () =>
    await element(by.id("add-new-password-button")).click();
  enterName = async (keys: string) =>
    await element(by.id("Secret Form.name")).sendKeys(keys);

  groupsSelect = () => element(by.className("multiple"));
  addGroup = () => element(by.className("highlighted"));

  enterUsername = async (keys: string) =>
    await element(by.id("Secret Form.username")).sendKeys(keys);
  enterPassword = async (keys: string) =>
    await element(by.id("Secret Form.password")).sendKeys(keys);
  submitByEnter = async () =>
    await element(by.id("Secret Form.password")).sendKeys(Key.ENTER);

  getSecretText = (title: string) => {
    return element(
      by.cssContainingText(".secret__title", `${title}`)
    ).getText();
  };

  getSecret = (title: string) => {
    return element(by.cssContainingText(".secret__title", `${title}`));
  };

  getAllSecretsElem = () => element.all(by.css(".secret-list-item"));

  getManageSecretToggleBtnElem = () => element(by.id("manageSecretBtn"));

  getPasswordNameElem = async (testBrowser: ProtractorBrowser) =>
    await testBrowser
      .element(by.id("Secret Form.password"))
      .getAttribute("value");

  getShowPasswordToggleBtnElem = (testBrowser: ProtractorBrowser) =>
    testBrowser.element(by.className("t-password-viewer__actions"));

  getSecretTitleElem = () => element(by.css(".secret__heading .secret__title"));

  getNewlyCreatedSecretElem = () =>
    element.all(by.css(".secret-list-item")).last();

  getToggleButton = (testBrowser: ProtractorBrowser, name: string) =>
    testBrowser
      .element(by.xpath(`//h3[contains(string(), "${name}")]`))
      .element(by.xpath(".."))
      .element(by.xpath("ancestor::div[1]"));
}
