import { browser, ExpectedConditions as EC } from "protractor";
import { AccountPage, LoginPage } from "./app.po";
import { SecretsPage } from "./secrets.po";
import { DEFAULT_WAIT, failLogin, login, register } from "./shared";

describe("Login and Register", () => {
  let loginPage: LoginPage;
  let accountPage: AccountPage;
  let secretsPage: SecretsPage;
  let USERNAME: string;
  let PASSWORD: string;

  beforeAll(() => {
    USERNAME =
      Math.random()
        .toString(36)
        .slice(2) + "@example.com";
    PASSWORD = "hunterhunter22";
  });

  beforeEach(() => {
    accountPage = new AccountPage();
    loginPage = new LoginPage();
    secretsPage = new SecretsPage();
  });

  afterAll(async () => {
    browser.executeScript("window.localStorage.clear();");
    browser.executeScript("window.sessionStorage.clear();");
  });

  it("Gives error message on incorrect login", async () => {
    await failLogin(browser, "testerrrr@example.com", "hunter2");
    const elem = loginPage.getErrors(browser);
    await EC.browser.wait(
      EC.presenceOf(elem),
      DEFAULT_WAIT,
      "Errors should have been shown"
    );
    await elem.isDisplayed();
  });

  it("Hit homepage and register", async () => {
    await register(browser, USERNAME, PASSWORD, PASSWORD);
  });

  it("navigates to change password", async () => {
    await login(browser, USERNAME, PASSWORD);
    await accountPage.navigateTo();
    await accountPage.resetPasswordButton().click();
  });

  it("shows an error if passwords don't match", async () => {
    await accountPage.enterOldPassword(PASSWORD);
    await accountPage.enterPassword("helloworld1");
    await accountPage.enterPasswordConfirm("helloworld2");
    const changeBtn = accountPage.changePassBtn();
    await EC.browser.wait(
      EC.presenceOf(changeBtn),
      100,
      "Submit button doesn't exist"
    );
    await changeBtn.click();
    const elem = accountPage.passwordErrors();
    await EC.browser.wait(
      EC.presenceOf(elem),
      10000,
      "Errors should have been shown"
    );
    await elem.isDisplayed();
    await accountPage.oldPassConfirmClear();
    await accountPage.passwordClear();
    await accountPage.passwordConfirmClear();
  });

  it("enters a new password", async () => {
    await accountPage.enterOldPassword(PASSWORD);
    await accountPage.enterPassword("helloworld1");
    await accountPage.enterPasswordConfirm("helloworld1");
    await accountPage.changePassBtn().click();
    const emailElem = loginPage.getEmailInput();
    await EC.browser.wait(
      EC.presenceOf(emailElem),
      10000,
      "Search secrets should be visible"
    );
    await emailElem.isDisplayed();
  });

  it("logged out and on login page", async () => {
    const currentUrl = await browser.getCurrentUrl();
    expect<any>(currentUrl).toEqual(browser.baseUrl + "login");
  });

  it("should login with new password", async () => {
    await login(browser, USERNAME, "helloworld1");
    const searchElem = secretsPage.getSearchElem();
    await EC.browser.wait(
      EC.presenceOf(searchElem),
      DEFAULT_WAIT,
      "Search secrets should be visible"
    );
    await searchElem.isDisplayed();
  });
});
