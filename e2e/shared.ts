import { browser, by, element } from "protractor";
import { ExpectedConditions as EC } from "protractor";
import { ProtractorBrowser } from "protractor/built/browser";
import {
  ConfirmEmailPage,
  isExtension,
  LoginPage,
  RegisterPage
} from "./app.po";

const SERVER_URL = "http://api:8000";
export const DEFAULT_WAIT = 37000;

export async function register(
  testBrowser: ProtractorBrowser,
  username: string,
  password: string,
  passwordConfirm: string,
  url = SERVER_URL
) {
  await testBrowser.sleep(50);
  const loginPage = new LoginPage();
  await loginPage.navigateTo(testBrowser);
  expect(await loginPage.getParagraphText(testBrowser)).toEqual("Log In");
  await loginPage.getRegisterLink(testBrowser).click();
  await browser.sleep(1000);

  const registerPage = new RegisterPage();
  expect(await registerPage.getParagraphText(testBrowser)).toEqual("Sign Up");
  await registerPage.enterEmail(testBrowser, username);
  await registerPage.clickEmailNextButton();
  await registerPage.enterPassword(testBrowser, password);
  await registerPage.enterPasswordConfirm(testBrowser, passwordConfirm);
  await registerPage.clickPasswordNextButton();
  if (isExtension) {
    await registerPage.enterServerURL(testBrowser, SERVER_URL);
  }
  const newsletterText = registerPage.getNewsletterText(testBrowser);
  await EC.browser.wait(
    EC.presenceOf(newsletterText),
    DEFAULT_WAIT,
    "Newsletter text should be shown"
  );
  await registerPage.clickNewsletterNextButton();
  const confirmEmailPage = new ConfirmEmailPage();
  const codeElem = confirmEmailPage.getCodeElem(testBrowser);
  await EC.browser.wait(
    EC.presenceOf(codeElem),
    DEFAULT_WAIT,
    "Confirm code page should be shown"
  );
  await codeElem.isDisplayed();
}

async function doLogin(
  testBrowser: ProtractorBrowser,
  username: string,
  password: string,
  url = SERVER_URL
) {
  await testBrowser.sleep(50);
  const loginPage = new LoginPage();
  await loginPage.navigateTo(testBrowser);
  await testBrowser.sleep(50);
  await loginPage.enterEmail(testBrowser, username);
  await loginPage.enterPassword(testBrowser, password);
  await loginPage.enterRememberMe(testBrowser);
  await browser.sleep(50);
  if (isExtension) {
    await loginPage.enterServerURL(testBrowser, SERVER_URL);
  }
  return await loginPage.submitByEnter(testBrowser);
}

/** This login will wait for success */
export async function login(
  testBrowser: ProtractorBrowser,
  username: string,
  password: string,
  url = SERVER_URL
) {
  await doLogin(testBrowser, username, password, url);
  return await browser.wait(
    EC.presenceOf(testBrowser.element(by.id("search"))),
    DEFAULT_WAIT
  );
}

/** This login will not wait for success - intended for testing for errors */
export async function failLogin(
  testBrowser: ProtractorBrowser,
  username: string,
  password: string,
  url = SERVER_URL
) {
  return await doLogin(testBrowser, username, password, url);
}

export const checkAndLogin = async (url: string) => {
  await browser.get(url);
  const USERNAME =
    Math.random()
      .toString(36)
      .slice(2) + "@example.com";
  const PASSWORD = "hunterhunter22";
  browser.controlFlow().execute(() => {
    browser.getCurrentUrl().then(currentUrl => {
      if (currentUrl === browser.baseUrl + "login") {
        register(browser, USERNAME, PASSWORD, PASSWORD);
        login(browser, USERNAME, PASSWORD);
        browser.wait(
          EC.stalenessOf(element(by.css("auth-form__heading"))),
          DEFAULT_WAIT,
          "Login wait failed"
        );
      }
    });
  });
};
