// tslint:disable:semicolon
import { browser, by, element, Key, ProtractorBrowser } from "protractor";
import { ExpectedConditions as EC } from "protractor";
import { DEFAULT_WAIT } from "./shared";

export const isExtension = browser.baseUrl.startsWith("chrome");

if (isExtension) {
  // this doesn't work for some reason with the extension
  browser.waitForAngularEnabled(false);
}

browser
  .manage()
  .timeouts()
  .setScriptTimeout(38 * 1000);

export class AppPage {
  clickPasswordsNavLinkElem = async () =>
    await element(by.partialLinkText("Passwords")).click();
  clickGroupsNavLinkElem = async () =>
    await element(by.partialLinkText("Groups")).click();
}

export class LoginPage {
  navigateTo = async (testBrowser: ProtractorBrowser) =>
    await testBrowser.get(testBrowser.baseUrl + "login");
  getParagraphText = async (testBrowser: ProtractorBrowser) =>
    await testBrowser.element(by.className("heading-medium")).getText();
  getEmailInput = () => element(by.id("Login Form.email"));
  getRegisterLink = (testBrowser: ProtractorBrowser) =>
    testBrowser.element(by.id("btn-signup"));
  enterEmail = async (testBrowser: ProtractorBrowser, keys: string) => {
    await testBrowser.sleep(50);
    const elem = testBrowser.element(by.id("Login Form.email"));
    await testBrowser.wait(
      EC.presenceOf(elem),
      DEFAULT_WAIT,
      "User email input should have been shown"
    );
    return await elem.sendKeys(keys);
  };
  enterPassword = async (testBrowser: ProtractorBrowser, keys: string) => {
    const elem = testBrowser.element(by.id("Login Form.password"));
    await testBrowser.wait(
      EC.presenceOf(elem),
      DEFAULT_WAIT,
      "password input should have been shown"
    );
    return await elem.sendKeys(keys);
  };
  enterRememberMe = async (testBrowser: ProtractorBrowser) => {
    const elem = testBrowser.element(by.className("form-field__checkbox"));
    return await elem.click();
  };
  enterServerURL = async (testBrowser: ProtractorBrowser, keys: string) =>
    await testBrowser.element(by.id("Login Form.password.url")).sendKeys(keys);
  submitByEnter = async (testBrowser: ProtractorBrowser) =>
    await testBrowser.element(by.id("Login Form.password")).sendKeys(Key.ENTER);
  getErrors = (testBrowser: ProtractorBrowser) =>
    testBrowser.element(by.className("auth-form__errors"));
  getProgressTextElem = (testBrowser: ProtractorBrowser) => {
    return testBrowser.element(by.className("save-progress"));
  };
}

export class RegisterPage {
  navigateTo = async () => await browser.get(browser.baseUrl + "/register");
  clickEmailNextButton = async () =>
    await element(by.id("email-button")).click();
  clickPasswordNextButton = async () =>
    await element(by.id("password-button")).click();
  clickNewsletterNextButton = async () =>
    await element(by.id("newsletter-button")).click();
  getParagraphText = async (testBrowser: ProtractorBrowser) =>
    await testBrowser.element(by.className("heading-medium")).getText();
  getNewsletterText = (testBrowser: ProtractorBrowser) =>
    testBrowser.element(by.className("register-step__newsletter-input"));
  enterEmail = async (testBrowser: ProtractorBrowser, keys: string) =>
    await testBrowser.element(by.id("Register Form.email")).sendKeys(keys);
  enterPassword = async (testBrowser: ProtractorBrowser, keys: string) =>
    await testBrowser.element(by.id("Register Form.password")).sendKeys(keys);
  enterPasswordConfirm = async (testBrowser: ProtractorBrowser, keys: string) =>
    await testBrowser
      .element(by.id("Register Form.passwordConfirm"))
      .sendKeys(keys);
  enterServerURL = async (testBrowser: ProtractorBrowser, keys: string) =>
    await testBrowser.element(by.id("userUrl")).sendKeys(keys);
  submitByEnter = async (testBrowser: ProtractorBrowser) => {
    const passInput = testBrowser.element(by.id("Register Form.password"));
    await browser.wait(
      EC.presenceOf(passInput),
      DEFAULT_WAIT,
      "Password Input should have been shown"
    );
    return await passInput.sendKeys(Key.ENTER);
  };
  getErrors = (testBrowser: ProtractorBrowser) =>
    testBrowser.element(by.className("auth-form__errors"));
}

export class ConfirmEmailPage {
  getCodeElem = (testBrowser: ProtractorBrowser) =>
    testBrowser.element(by.id("code"));
  getParagraphText = async () => await element(by.css("app-root h2")).getText();
  enterEmail = async (keys: string) =>
    await element(by.id("userEmail")).sendKeys(keys);
  enterPassword = async (keys: string) =>
    await element(by.id("userPassword")).sendKeys(keys);
  enterPasswordConfirm = async (keys: string) =>
    await element(by.id("passwordConfirm")).sendKeys(keys);
  enterServerURL = async (keys: string) =>
    await element(by.id("userUrl")).sendKeys(keys);
  submitByEnter = async () =>
    await element(by.id("userPassword")).sendKeys(Key.ENTER);
  getErrors = () => element(by.className("auth-form__errors"));
}

export class AccountPage {
  navigateTo = async () => await browser.get(browser.baseUrl + "/account");
  navigateToChangePass = async () =>
    await browser.get(browser.baseUrl + "/account/change-password");
  navigateToPassword = async () =>
    await browser.get(browser.baseUrl + "/account/change-password");
  resetPasswordButton = () =>
    element(
      by.cssContainingText(".account__item-heading", "Change Account Password")
    );
  enterOldPassword = async (keys: string) =>
    await element(by.id("Change Password Form.oldPassword")).sendKeys(keys);
  oldPassConfirmClear = async () =>
    await element(by.id("Change Password Form.oldPassword")).clear();
  enterPassword = async (keys: string) =>
    await element(by.id("Change Password Form.newPassword")).sendKeys(keys);
  passwordClear = async () =>
    await element(by.id("Change Password Form.newPassword")).clear();
  enterPasswordConfirm = async (keys: string) =>
    await element(by.id("Change Password Form.newPasswordConfirm")).sendKeys(
      keys
    );
  passwordConfirm = () =>
    element(by.id("Change Password Form.newPasswordConfirm"));
  passwordConfirmClear = async () =>
    await element(by.id("Change Password Form.newPasswordConfirm")).clear();
  changePassBtn = () => element(by.id("changePass"));
  passwordErrors = () => element(by.className("form-field__error"));
  logout = () => element(by.id("btn-logout")).click();
}
